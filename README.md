# cf-ingress-operator

## Purpose
(Cloudflare)[https://cloudflare.com] provides free so-called Argo Tunnels aka Zero Trust tunnels which allow to expose an on-prem application to internet.

Tunnels route incoming traffic destined for a CNAME in a CF-controlled zone.
See https://letsdocloud.com/2021/06/expose-kubernetes-service-using-cloudflare-argo-tunnel/ for how it can be used to expose applications running in your on-prem cluster.

This operator scans Ingress objects matching a selector and creates service of type ExternalName, which is picked up by (External-DNS)[https://github.com/kubernetes-sigs/external-dns/] which in turn should create a proper CNAME in a Cloudflare-hosted zone.

## Prerequisites:

1. A kubernetes clusteer
2. External-DNS configured for Cloudflare
3. Ingress-controller (I've tested with nginx-ingress-controller but should work with any)
4. Cloudflared running and configured to point to the underlying service of your ingress-controller (see https://github.com/kubernetes-sigs/external-dns/). Proper helm chart for this configuration is planned.
